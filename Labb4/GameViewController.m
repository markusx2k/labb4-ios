//
//  GameViewController.m
//  Labb4
//
//  Created by IT-Högskolan on 2015-02-09.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "GameViewController.h"


@interface GameViewController ()
@property (weak, nonatomic) IBOutlet UIView *playerPad;
@property (nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (weak, nonatomic) IBOutlet UIView *pongBall;
@property (nonatomic) double xSpeed;
@property (nonatomic) double ySpeed;
@property (nonatomic) double speed;
@property (nonatomic) NSInteger sizeFromCenter;
@property (nonatomic) double angleModifier;
@property (weak, nonatomic) IBOutlet UIView *AIPad;
@property (weak, nonatomic) IBOutlet UILabel *playerScore;
@property (weak, nonatomic) IBOutlet UILabel *AIScore;
@property (nonatomic) AVAudioPlayer *wallHitPlayer;
@property (nonatomic) AVAudioPlayer *padHitPlayer;
@property (nonatomic) AVAudioPlayer *winSoundPlayer;
@property (nonatomic) AVAudioPlayer *loseSoundPlayer;

@end

@implementation GameViewController

- (IBAction)viewPan:(UIPanGestureRecognizer *)sender {
    CGPoint translation = [sender translationInView:self.view];
    float diff = translation.x*1.3;
    self.playerPad.center = CGPointMake(self.playerPad.center.x+diff, self.playerPad.center.y);
    [sender setTranslation:CGPointMake(0, 0) inView:self.view];
    
    if (self.playerPad.center.x < self.sizeFromCenter) {
        self.playerPad.center = CGPointMake(self.sizeFromCenter, self.playerPad.center.y);
    }
    if (self.playerPad.center.x > self.view.frame.size.width - self.sizeFromCenter) {
        self.playerPad.center = CGPointMake(self.view.frame.size.width - self.sizeFromCenter, self.playerPad.center.y);
    }
}
- (IBAction)go:(id)sender {
    self.goButton.hidden = YES;
    [self startTimer];
}
-(void)startTimer {
    self.speed = 1;
    double x = arc4random() % 10 +1;
    double y = arc4random() % 10 +5;
    
    if (arc4random() % 2 > 0) {
        x = -x;
    }
    
    self.xSpeed = x/10;
    self.ySpeed = y/10;
    self.timer = [NSTimer
                  scheduledTimerWithTimeInterval:0.01
                  target:self
                  selector:@selector(move)
                  userInfo:nil repeats:YES];
}
-(void)startOtherTimer {
    self.timer = [NSTimer
                  scheduledTimerWithTimeInterval:3
                  target:self
                  selector:@selector(startTimer)
                  userInfo:nil repeats:NO];
}
- (void) stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)move {
    self.pongBall.center = CGPointMake(self.pongBall.center.x + self.xSpeed, self.pongBall.center.y + self.ySpeed);
    if (!CGRectContainsPoint(self.view.frame, self.pongBall.center)) {
        //AudioServicesPlaySystemSound(winSoundID);
        
        if (self.pongBall.center.x > self.view.frame.size.width) {
            self.xSpeed = -self.xSpeed;
            [self.wallHitPlayer play];
        } else if (self.pongBall.center.x < 0) {
            self.xSpeed = fabs(self.xSpeed);
            [self.wallHitPlayer play];
        }
        
        if (self.pongBall.center.y > self.AIPad.center.y) {
            [self stopTimer];
            //self.goButton.hidden = NO;
            self.pongBall.center = self.view.center;
            self.playerScore.text = [[NSString alloc] initWithFormat: @"%d", [self.playerScore.text integerValue]+1];
            [self.winSoundPlayer play];
            [self startOtherTimer];
        } else if (self.pongBall.center.y < self.playerPad.center.y) {
            [self stopTimer];
            //self.goButton.hidden = NO;
            self.pongBall.center = self.view.center;
            self.AIScore.text = [[NSString alloc] initWithFormat: @"%d", [self.AIScore.text integerValue]+1];
            [self.loseSoundPlayer play];
            [self startOtherTimer];
        }
            
        
    }
    
    if (CGRectContainsPoint(self.playerPad.frame, self.pongBall.center)) {
        self.speed = self.speed + 0.5;
        
        self.angleModifier = self.pongBall.center.x - self.playerPad.center.x;
        if (self.angleModifier < 5 && self.angleModifier > -5) {
            self.angleModifier = 5;
        }
        self.ySpeed = fabs((self.speed*10)/self.angleModifier);
        self.xSpeed = (self.speed*self.angleModifier)/20;
        [self.padHitPlayer play];
    }
    
    if (CGRectContainsPoint(self.AIPad.frame, self.pongBall.center)) {
        self.ySpeed = -self.speed;
        [self.padHitPlayer play];
    }
    
    if (self.AIPad.center.x > self.pongBall.center.x) {
        self.AIPad.center = CGPointMake(self.AIPad.center.x -3, self.AIPad.center.y);
    } else if (self.AIPad.center.x < self.pongBall.center.x) {
        self.AIPad.center = CGPointMake(self.AIPad.center.x +3, self.AIPad.center.y);
    }
    if (self.AIPad.center.x < self.sizeFromCenter) {
        self.AIPad.center = CGPointMake(self.sizeFromCenter, self.AIPad.center.y);
    }
    if (self.AIPad.center.x > self.view.frame.size.width - self.sizeFromCenter) {
        self.AIPad.center = CGPointMake(self.view.frame.size.width - self.sizeFromCenter, self.AIPad.center.y);
    }
   
}


-(void)viewDidLayoutSubviews {
    self.playerPad.center = CGPointMake(self.view.center.x, self.view.frame.size.height/15);
    self.AIPad.center = CGPointMake(self.view.center.x, self.view.frame.size.height - self.view.frame.size.height/20);
    self.pongBall.center = self.view.center;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sizeFromCenter = self.playerPad.frame.size.width/2;
    NSURL *wallHitUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"wallHit" ofType:@"mp3"]];
    NSURL *padHitUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"padHit" ofType:@"wav"]];
    NSURL *winSoundUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"winSound" ofType:@"wav"]];
    NSURL *loseSoundUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"loseSound" ofType:@"mp3"]];
    self.wallHitPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:wallHitUrl error:nil];
    self.padHitPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:padHitUrl error:nil];
    self.winSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:winSoundUrl error:nil];
    self.loseSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:loseSoundUrl error:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
